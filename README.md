# How to Install Tensorflow 2.4 on Ubuntu 18.04 LTS with GPU support: Nvidia Drivers, CUDA 11 and cuDNN

## Step 1: Checking which versions of drivers and software to install for mutual compatibility with Tensorflow

```
1. NVIDIA GPU drivers — CUDA 11.4 requires 450.x or higher 
2. CUDA® Toolkit — TensorFlow supports CUDA 11.4 (TensorFlow >= 2.4.0). Tensorflow 1.13 and above requires CUDA 10.* I would like to be able to install various versions of Tensorflow (with GPU support) between 1.13–2.1, so CUDA 10.1 is definitely required
3. CUPTI (ships with the CUDA Toolkit) 
4. g++ compiler and toolchain
5. cuDNN SDK (>= 8.2)
```

```
lspci | grep -i nvidia
```
<p align="center">
    <img src="images/cuda_capable.png" alt="grep" width="700"/>
</p>
